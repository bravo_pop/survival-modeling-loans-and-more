import pandas as pd
import os
import zipfile
import numpy as np
import re
from plotnine import *

# set file lookups
your_dir = '<INSERT YOUR FREDDIE MAC ZIP DOWNLOAD PATH HERE>'

# find zip
dir_of_interest = os.listdir(your_dir)
fhms_file = your_dir + list(filter(lambda x: re.search("MLPD_datamart\.zip$", x), dir_of_interest))[0]

# look at zip
zip_file = zipfile.ZipFile(fhms_file)
zip_contents = zip_file.infolist()

# read csv
fhms_data = pd.read_csv(zip_file.open('MLPD_2019_csv_format.csv'))

# count obs and group by quarterly so see # of quarters
loan_obs_count = fhms_data.groupby(['lnno'])['quarter'].count().reset_index(name = "n")
loan_obs_count['n'] = loan_obs_count['n'] / 4

# create 'ze plot
loan_count_plot = (
    ggplot(loan_obs_count, aes(x = 'n') ) + 
    geom_histogram(binwidth = 1, color = 'white') +
    scale_y_continuous(expand = [0,0,0,0]) +
    theme_bw() +
    labs(title = 'Years of Observations for Unique Loans',
         x = 'Years of Observations',
         y = 'Count Unique Loans') + 
    theme(
        plot_title = element_text(ha = 'center')
        )
)

print(loan_count_plot)

# now onto prop types, first let's count
fhms_data['prop_type'] = np.where(pd.isna(fhms_data['Code_sr']), 'Multifamily', 'Senior Housing')
prop_types = fhms_data.groupby(['prop_type'], dropna=False)['lnno'].count().reset_index(name = 'n')
prop_types = prop_types.sort_values(by = 'n', ascending=True)
prop_types['prop_type'] = pd.Categorical(prop_types['prop_type'], categories = prop_types['prop_type'])

print(prop_types)
