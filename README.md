# Survival Modeling Loans and More

This project houses a set of data access scripts and models built to help better understand survival modeling in multiple contexts. The primary context of interest will be commercial real estate loans.

Part 1 | Getting the data

1. Navigate to this link: https://mf.freddiemac.com/investors/data.html
2. Download https://mf.freddiemac.com/docs/MLPD_datamart.zip

Part 2 | Ingestion

1. I've put an R script and a Python script up for parsing
2. You'll find a simple plot output of yearly observations for each loan (the length of life in years)
3. You'll also find a property type count...there should only be two (multifamily and senior housing).